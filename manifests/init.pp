# == Class: facterpush
#
# Full description of class facterpush here.
#
# === Parameters
#
# Document parameters here.
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
#
# === Examples
#
#  class { 'facterpush':
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class facterpush {
$myfacts = [ '/etc/facter/facts.d/environment.txt' ]

  file { '/etc/facter' :
    ensure => directory,
    owner  => 'root',
    group  => 'root',
  }

  file { '/etc/facter/facts.d' :
    ensure  => directory,
    owner   => 'root',
    group   => 'root',
    purge   => true,
    force   => true,
    source  => "puppet:///${::environment}/facts.d",
    recurse => true,
  }
  file { $myfacts :
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
  }
}
