# facterpush

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with facterpush](#setup)
    * [What facterpush affects](#what-facterpush-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with facterpush](#beginning-with-facterpush)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

Create a custom fact called instance

## Module Description

Create a custom fact called instance

## Setup

### What facterpush affects

Creates /etc/facter/facts.d/{INSTANCE.txt,ENVIRONMENT.txt}

### Setup Requirements **OPTIONAL**


### Beginning with facterpush
  
  Add the following to site.pp
   
    class {'facterpush'
         
    }

## Usage


## Reference


## Limitations


## Development


## Release Notes/Contributors/Etc **Optional**
